package com.example.cake

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.view.ViewTreeObserver
import androidx.core.view.GestureDetectorCompat
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fan_layout.*
import kotlinx.android.synthetic.main.fan_layout.view.*

class MainActivity : AppCompatActivity(){

    private lateinit var mDetector : GestureDetectorCompat
    private lateinit var mDialog : BasicDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initDataSet()
        initScrollSet()
    }

    private fun initDataSet() {
        /*서버로 부터 전달받아서 표시해야할 데이터들*/
        val list = ArrayList<DataItems>()
        list.add(DataItems(getDrawable(R.drawable.iron)!!,"아이언맨"))
        list.add(DataItems(getDrawable(R.drawable.doctor)!!,"닥터 스트레인지"))
        list.add(DataItems(getDrawable(R.drawable.hulk)!!,"헐크"))
        list.add(DataItems(getDrawable(R.drawable.black)!!,"블랙 위도우"))
        list.add(DataItems(getDrawable(R.drawable.nebula)!!,"네뷸라"))
        list.add(DataItems(getDrawable(R.drawable.rocket)!!,"로켓"))
        list.add(DataItems(getDrawable(R.drawable.star)!!,"스타로드"))
        list.add(DataItems(getDrawable(R.drawable.iron)!!,"아이언맨"))
        list.add(DataItems(getDrawable(R.drawable.doctor)!!,"닥터 스트레인지"))
        list.add(DataItems(getDrawable(R.drawable.hulk)!!,"헐크"))
        list.add(DataItems(getDrawable(R.drawable.black)!!,"블랙 위도우"))
        list.add(DataItems(getDrawable(R.drawable.nebula)!!,"네뷸라"))
        list.add(DataItems(getDrawable(R.drawable.rocket)!!,"로켓"))
        list.add(DataItems(getDrawable(R.drawable.star)!!,"스타로드"))
        list.add(DataItems(getDrawable(R.drawable.iron)!!,"아이언맨"))
        list.add(DataItems(getDrawable(R.drawable.doctor)!!,"닥터 스트레인지"))
        list.add(DataItems(getDrawable(R.drawable.hulk)!!,"헐크"))
        list.add(DataItems(getDrawable(R.drawable.black)!!,"블랙 위도우"))
        list.add(DataItems(getDrawable(R.drawable.nebula)!!,"네뷸라"))
        list.add(DataItems(getDrawable(R.drawable.rocket)!!,"로켓"))
        list.add(DataItems(getDrawable(R.drawable.star)!!,"스타로드"))
        list.add(DataItems(getDrawable(R.drawable.iron)!!,"아이언맨"))
        list.add(DataItems(getDrawable(R.drawable.doctor)!!,"닥터 스트레인지"))
        list.add(DataItems(getDrawable(R.drawable.hulk)!!,"헐크"))
        list.add(DataItems(getDrawable(R.drawable.black)!!,"블랙 위도우"))
        list.add(DataItems(getDrawable(R.drawable.nebula)!!,"네뷸라"))
        list.add(DataItems(getDrawable(R.drawable.rocket)!!,"로켓"))
        list.add(DataItems(getDrawable(R.drawable.star)!!,"스타로드"))
        list.add(DataItems(getDrawable(R.drawable.iron)!!,"아이언맨"))
        list.add(DataItems(getDrawable(R.drawable.doctor)!!,"닥터 스트레인지"))
        list.add(DataItems(getDrawable(R.drawable.hulk)!!,"헐크"))
        list.add(DataItems(getDrawable(R.drawable.black)!!,"블랙 위도우"))
        list.add(DataItems(getDrawable(R.drawable.nebula)!!,"네뷸라"))
        list.add(DataItems(getDrawable(R.drawable.rocket)!!,"로켓"))
        list.add(DataItems(getDrawable(R.drawable.star)!!,"스타로드"))

        // 데이터 표시할 어뎁터 생성
        val adapter = ListAdapter(list)
        // 리사이클러 뷰에 어뎁터 세팅
        recyclerView.adapter = adapter

        // 제스쳐 어뎁더에 매핑
        mDetector = GestureDetectorCompat(this,MyGetureListener(recyclerView,adapter))
    }


    private fun initScrollSet() {
        // 스크롤 이벤트 감지 - 스크롤 완료 됬을때 아이템 가져온다
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                // newState == SCROLL_STATE_IDLE  == 0 정지
                if(newState== RecyclerView.SCROLL_STATE_IDLE){
                    Log.e("onFling", recyclerView.image_title.text.toString())
                    //showCustomDialog()
                    //recyclerView.scrollToPosition(0)
                }
            }
        })
    }

    private fun showCustomDialog() {
        mDialog = BasicDialog(this)
        mDialog.show()
    }


    override fun onTouchEvent(event: MotionEvent?): Boolean {
        // 액티비티에서 터치 감시지 제스쳐 매핑
        mDetector.onTouchEvent(event)
        return super.onTouchEvent(event)
    }
}
