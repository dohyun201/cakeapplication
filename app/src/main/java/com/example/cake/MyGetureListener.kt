package com.example.cake

import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MyGetureListener(view :RecyclerView,adapter : ListAdapter) : GestureDetector.OnGestureListener {

    var recyelcerViews = view
    var adapaters = adapter

    override fun onShowPress(event: MotionEvent?) {
        Log.e("onShowPress", "onShowPress: $event") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSingleTapUp(event: MotionEvent?): Boolean {
        Log.e("onSingleTapUp", "onSingleTapUp: $event")
        // 랜덤함수 사용해서 , 임의숫자 표시되게함
        val random = Random()
        val num = random.nextInt(adapaters.itemCount)
        recyelcerViews.smoothScrollToPosition(num)
        return true
    }

    override fun onDown(event: MotionEvent?): Boolean {
        Log.e("onDown", "onDown: $event")
        return true
    }

    override fun onFling(event1: MotionEvent?, event2: MotionEvent?, velocityX :Float, velocityY: Float): Boolean {
        Log.e("onFling", "onFling: $event1 $event2")
        return true
    }

    override fun onScroll(event1: MotionEvent?, event2: MotionEvent?, p2: Float, p3: Float): Boolean {
//        Log.e("onScroll", "onScroll: $event1 $event2")
        return true
    }

    override fun onLongPress(event: MotionEvent?) {
        Log.e("onLongPress", "onLongPress: $event")
    }


}
