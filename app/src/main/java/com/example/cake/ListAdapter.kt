package com.example.cake

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fan_layout.view.*


// 리스트 넘어옴
class ListAdapter (private val items: ArrayList<DataItems>) :
        RecyclerView.Adapter<Listviewholder>() {

    override fun onBindViewHolder(holder: Listviewholder, position: Int) {
        // 이미지 , 이름 세팅
        var getImage = items[position].image
        var getTitle = items[position].title
        holder.itemView.listImage.setImageDrawable(getImage)
        holder.itemView.image_title.setText(getTitle)

    }

    override fun getItemCount(): Int {
        // 전체갯수 표시하기 위함
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Listviewholder {
        // 반복할 xml
        val inflateView = LayoutInflater.from(parent.context).inflate(R.layout.fan_layout, parent, false)
        return Listviewholder(inflateView)
    }


}
