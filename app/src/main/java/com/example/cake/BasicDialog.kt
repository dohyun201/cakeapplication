package com.example.cake

import android.app.Dialog
import android.content.Context
import android.os.Bundle

class BasicDialog(context: Context) : Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_layout)
    }
}
